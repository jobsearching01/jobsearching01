# **👋 Welcome, to my GitLab!**

<br />

## 🧪 [Labs](https://gitlab.com/jobsearching01/labs)

### 💻 IT Labs

- **Active Directory - Create 1,000 Users & Setup DHCP For Client Log-in** -> [ad-create-1000-users-and-dhcp](https://gitlab.com/jobsearching01/labs/-/tree/main/it-labs/ad-create-1000-users-and-dhcp) 

## 🛠️ [Projects](https://gitlab.com/jobsearching01/projects)

- **Chntpw - Clear Windows local account password with Linux** -> [chntpw](https://gitlab.com/jobsearching01/projects/-/tree/main/chntpw)

## ⏩ [Scripts](https://gitlab.com/jobsearching01/scripts)

### ⚡ PowerShell

- **Batch Active Directory User Creation (WS 2019)** -> [my-create-users.ps1](https://gitlab.com/jobsearching01/scripts/-/blob/main/powershell/my-create-users.ps1)
- **Personal Post Install (Win 10)** -> [postinstall-personal.ps1](https://gitlab.com/jobsearching01/scripts/-/blob/main/powershell/postinstall-personal.ps1)

### ⚙️ Batch

- **Decrease BitLocker MBAM client wait time to 2 minutes (Win 10)** -> [fast-mbam-client.bat](https://gitlab.com/jobsearching01/scripts/-/blob/main/batch/fast-mbam-client.bat)
- **Install .NET Framework 3.5 (Win 10)** -> [dotnet3-install.bat](https://gitlab.com/jobsearching01/scripts/-/blob/main/batch/dotnet3-install.bat)
- **Fixing File Explorer (Win 10)** -> [explorer-config.bat](https://gitlab.com/jobsearching01/scripts/-/blob/main/batch/explorer-config.bat)

### 💲_ Bash

- **Install all Android APK files in directory** -> [batch-apk-install.sh](https://gitlab.com/jobsearching01/scripts/-/blob/main/bash/batch-apk-install.sh)
- **Compress all mp4 files in directory** -> [batch-compress-mp4.sh](https://gitlab.com/jobsearching01/scripts/-/blob/main/bash/batch-compress-mp4.sh)

## 📂 [Tips & Tricks](https://gitlab.com/jobsearching01/tips-n-tricks/-/blob/main/README.md)

- **Windows** -> [click here](https://gitlab.com/jobsearching01/tips-n-tricks#windows)
- **Linux** -> [click here](https://gitlab.com/jobsearching01/tips-n-tricks#linux)
- **MacOS** -> [click here](https://gitlab.com/jobsearching01/tips-n-tricks#macos)
